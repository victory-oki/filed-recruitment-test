import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import * as fromPayments from '../state/payment/payments.reducer';
import { IPayment } from '../services/payment.service';
import { BaseComponent } from '../shared/base-component/base-component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['holder', 'no', 'cvv', 'expiry', 'amount'];
  dataSource = new MatTableDataSource<IPayment>([]);
  public isLoading = true;
  routeStatePayload: { [k: string]: any; };
  transactions: IPayment[];
  balance = 0;

  constructor(
    private router:Router,
    private snackBar:MatSnackBar,
    private store:Store<fromPayments.paymentState>
  ) { 
    super()
    this.routeStatePayload = this.router.getCurrentNavigation().extras.state;
    if(this.routeStatePayload?.message){
      this.openSnackBar(this.routeStatePayload.message)
    }
  }

  ngOnInit(): void {
    this.addSubscription(
      this.store.pipe(select(fromPayments.getTransactions)).subscribe(
        transactions=>{
          this.transactions = transactions
          this.dataSource = new MatTableDataSource<IPayment>(this.transactions);
          console.log(transactions)
        }
      )
    )
    this.addSubscription(
      this.store.pipe(select(fromPayments.getBalance)).subscribe(
        balance=>{
          this.balance = balance
          console.log(balance)
        }
      )
    )
  }

  openSnackBar(message?: string) {
    this.snackBar.open(message, "close", {
      verticalPosition: "top",
      horizontalPosition: "center",
    });
  }

  ngOnDestroy(){
    this.clearSubscription();
  }
}

const ELEMENT_DATA: any[] = [
  {holderName: "Dr. Mathias Oko Offoboche",
  cardNo: '2240 4904 0309 9390', amount: 2348},
];
