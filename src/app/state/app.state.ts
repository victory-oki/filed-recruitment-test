import { paymentState } from "./payment/payments.reducer";

export interface State{
    payments: paymentState
}