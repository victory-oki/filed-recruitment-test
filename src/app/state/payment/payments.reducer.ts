import { createFeatureSelector, createSelector } from "@ngrx/store"
import { IPayment } from "src/app/services/payment.service"
import * as fromRoot from '../app.state'
import {PaymentActions, PaymentActionTypes} from './payments.action'
export interface paymentState{
    transactions: IPayment[]
}
const initialState:paymentState = {
    transactions:[]
}
const getPaymentsFeatureState = createFeatureSelector<paymentState>('payments');
export const getTransactions = createSelector(
    getPaymentsFeatureState,
    state=> state.transactions
) 
export const getBalance = createSelector(
    getTransactions,
    transactions=>transactions.reduce((acc,transaction)=> {return acc + transaction.amount},0)
)
export function paymentReducer(state=initialState,action:PaymentActions):paymentState{
    switch(action.type){
        case PaymentActionTypes.UpdatePaymentTransactions:
            let transactions = [...state.transactions, action.payload]
            return {
                ...state,
                transactions
            }
        default:
            return state
    }
}