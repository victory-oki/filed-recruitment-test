import { Action } from "@ngrx/store";

export enum PaymentActionTypes {
    UpdatePaymentTransactions = '[Payment] Update Payment Transactions',
}

export class UpdatePaymentTransactions implements Action{
    readonly type = PaymentActionTypes.UpdatePaymentTransactions;
    constructor(public payload: any){}
}

export type PaymentActions = | UpdatePaymentTransactions