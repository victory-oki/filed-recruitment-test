import { paymentReducer } from "./payment/payments.reducer";

export const rootReducer = {
    payments: paymentReducer
};