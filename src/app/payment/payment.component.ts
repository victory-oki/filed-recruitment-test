import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { catchError, retry } from 'rxjs/operators';
import { PaymentService } from '../services/payment.service';
import { BaseComponent } from '../shared/base-component/base-component';
/*NGRX*/
import * as paymentActions from '../state/payment/payments.action';
import * as fromPayments from '../state/payment/payments.reducer';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent extends BaseComponent implements OnInit, OnDestroy {
  paymentForm: FormGroup
  constructor(
    private router:Router,
    private fb:FormBuilder, 
    private snackBar:MatSnackBar,
    private PaymentService:PaymentService, 
    private store:Store<fromPayments.paymentState>,
  ) { 
    super()
    this.paymentForm = this.fb.group({
      cardHolder: ['',Validators.required],
      cardNumber:['',[Validators.required, this.numberValidation]],
      amount:['',[Validators.required, Validators.min(1)]],
      expiry:['',[Validators.required, this.expiryValidation]],
      cvv:['',[Validators.minLength(3), Validators.maxLength(3), this.numberValidation]],
    })
  }

  
  ngOnInit(): void {}

  // GETTER FUNCTIONS
  get cardHolder() {
    return this.paymentForm.get("cardHolder");
  }
  get cardNumber() {
    return this.paymentForm.get("cardNumber");
  }
  get amount() {
    return this.paymentForm.get("amount");
  }
  get expiry() {
    return this.paymentForm.get("expiry");
  }
  get cvv() {
    return this.paymentForm.get("cvv");
  }

  // NUMBER VALIDATION
  private numberValidation(control: FormControl): { [key: string]: any } {
    return !isNaN(control.value)? null: {isnan:true}
  }

  // EXPIRY VALIDATION
  private expiryValidation(control: FormControl): { [key: string]: any } {
    let month, year
    let isValidExpiry = false

    const isValidDate = (month,year)=>{
      console.log(month,year)
      return !isNaN(year) && !isNaN(month) && month > 0 && month < 13
    }

    const isValidExpiration = (month,year)=>{
      let today = new Date();
      let curYear = today.getFullYear()
      let curMonth= today.getMonth()+1
      console.log(month,year,"2")
      return  (year*100 + month) > (curYear*100 + curMonth)
    }

    if(control.value.length == 7){
      [month,year] = control.value.split("/")
      isValidExpiry = (isValidDate(+month,+year) && isValidExpiration(+month,+year))? true : false
    }
    else{
      isValidExpiry = false
    }
    return isValidExpiry? null: {isInvalid:true}
  }

  //ON SUBMIT FUNCTION
  submit(value){
    const [month,year] = value.expiry.split("/")
    let data = {
      ...value,
      expiry: new Date(year, month-1)
    }
    this.loading = true;
    this.addSubscription(
      this.PaymentService.makePayment(data)
      .pipe(
        retry(1),
        catchError(() => {
          throw { message: "An error occured"};
        })
      ).subscribe(
        _=>{
          console.log(data)
          this.loading = false;
          this.store.dispatch(new paymentActions.UpdatePaymentTransactions(data))
          this.router.navigate(['home'], { state: { message: 'Payment has been made successfully' } });
        },
        ({message})=>{
          this.loading = false;
          this.openSnackBar(`${message}`)
        }
      )
    )
  }

  //UTILITY FUNCTIONS
  openSnackBar(message?: string) {
    this.snackBar.open(message, "close", {
      verticalPosition: "top",
      horizontalPosition: "center",
    });
  }
  expiryDateHandler(event){
    let el = event.target as HTMLInputElement
    let curLength = el.value.length;
    if(curLength === 2){
      let newInput = el.value;
      newInput += '/';
      el.value = newInput;
    }
  }
  isRequiredError(control:AbstractControl){
    return control.errors?.required && (control.dirty || control.touched)
  }
  isInvalidError(control:AbstractControl){
    return !control.errors?.required && (control.dirty || control.touched) && control.errors?.isInvalid
  }
  isAmountMin(control:AbstractControl){
    return !control.errors?.required && (control.dirty || control.touched) && control.hasError('min')
  }
  isMinLength(control:AbstractControl){
    return !control.errors?.required && (control.dirty || control.touched) && control.hasError('minlength')
  }
  isMaxLength(control:AbstractControl){
    return !control.errors?.required && (control.dirty || control.touched) && control.hasError('maxlength')
  }
  isNotNumber(control:AbstractControl){
    return !control.errors?.required && (control.dirty || control.touched) && control.hasError('isnan')
  }

  ngOnDestroy(){
    this.clearSubscription();
  }
}
