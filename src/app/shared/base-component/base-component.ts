import { Subscription, TeardownLogic, BehaviorSubject } from 'rxjs';

export class BaseComponent {
  public subscription = new Subscription();
  public loading: Boolean = false;
  public loading$: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);

  addSubscription(logic: TeardownLogic): void {
    this.subscription.add(logic);
  }

  clearSubscription(): void {
    this.subscription.unsubscribe();
  }
}
