import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PaymentComponent } from './payment/payment.component';


const routes: Routes = [
  {
    path:'home', component:HomeComponent,
  },
  {
    path: 'payments',
    component: PaymentComponent
  },
  {
    path:'', redirectTo:'home', pathMatch:'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
