import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  private baseUrl = `${environment.api_base_url}`;
  constructor(private http: HttpClient) { }

  makePayment(body:IPayment): Observable<any> {
    const requestUrl = `${this.baseUrl}/transactions`;
    return this.http.post(requestUrl,body);
  }
}


export interface IPayment{
  cardHolder: string;
  cardNumber:string,
  amount:number,
  expiry:Date,
  cvv?:string,
}